//mostrar los 12 meses, pidiendole al usuario un numero y muestre el mes correspondiente
#include <iostream>

using namespace std;

int main()
{

    /*int numero;
    cout<<"digite un numero entre 1 y 12 para conocer el mes: ";
    cin>>numero;
    switch(numero){
        case 1: cout<<"mes de enero";break;
        case 2: cout<<"mes de febrero";break;
        case 3: cout<<"mes de marzo";break;
        case 4: cout<<"mes de abril";break;
        case 5: cout<<"mes de mayo";break;
        case 6: cout<<"mes de junio";break;
        case 7: cout<<"mes de julio";break;
        case 8: cout<<"mes de agosto";break;
        case 9: cout<<"mes de septiembre";break;
        case 10: cout<<"mes de octubre";break;
        case 11: cout<<"mes de noviembre";break;
        case 12: cout<<"mes de diciembre";break;
        default: cout<<"el numero no corresponde a ningun mes";
    }*/
    //convertir un numero entero con el mismo valor pero expresado en numeros romanos
    //M=1000,D=500,C=100,L=50,X=10
    /*int num, unidades, decenas, centenas, millar;
    cout<<"                     conversion de un numero natural a romanos"<<endl;
    cout<<"digite el numero a convertir: ";
    cin>>num;
    //dividiendo el numero en uni, dec, cent, mill
    //2341  2  3  4  1
    unidades=num%10;//unidades=1
    num=num/10;//num=234(forma de escribir num/=10)
    decenas=num%10;//decenas=4
    num/=10;//num=23
    centenas=num%10;//centenas=3
    num/=10;//num = 2
    millar=num%10;//millar 2
    num/=10;//0
    switch (millar){
        case 1: cout<<"M";break;
        case 2: cout<<"MM";break;
        case 3: cout<<"MMM";break;
    }
    switch (centenas){
        case 1: cout<<"C"; break;
        case 2: cout<<"CC"; break;
        case 3: cout<<"CCC"; break;
        case 4: cout<<"CD"; break;
        case 5: cout<<"D"; break;
        case 6: cout<<"DC"; break;
        case 7: cout<<"DCC"; break;
        case 8: cout<<"DCCC"; break;
        case 9: cout<<"CM"; break;
    }
    switch (decenas){
        case 1: cout<<"X"; break;
        case 2: cout<<"XX"; break;
        case 3: cout<<"XXX"; break;
        case 4: cout<<"XL"; break;
        case 5: cout<<"L"; break;
        case 6: cout<<"LX"; break;
        case 7: cout<<"LXX"; break;
        case 8: cout<<"LXXX"; break;
        case 9: cout<<"XC"; break;
    }
    switch(unidades){
        case 1: cout<<"I"; break;
        case 2: cout<<"II"; break;
        case 3: cout<<"III"; break;
        case 4: cout<<"IV"; break;
        case 5: cout<<"V"; break;
        case 6: cout<<"VI"; break;
        case 7: cout<<"VII"; break;
        case 8: cout<<"VIII"; break;
        case 9: cout<<"IX"; break;
    }*/
    //SIMULACION DE UN CAJERO AUTOMATICO AHORRO = 1000$
    int saldoini=1000,opc;
    float extra, saldo=0, retiro;
    cout<<"                 Bienvenido al cajero virtual "<<endl;
    cout<<"1.- Ingresar dinero en la cuenta "<<endl;
    cout<<"2.- Retiro de dinero"<<endl;
    cout<<"3.- Consulta de saldo"<<endl;
    cout<<"4.- Salir "<<endl;
    cout<<"Opcion: ";
    cin>>opc;
    switch(opc){
        case 1: cout<<"cuanto dinero desea ingresar en la cuenta: ";
        cin>>extra;
        saldo=saldoini+extra;
        cout<<"\nDinero en cuenta es: "<<saldo;
        break;
        case 2: cout<<"cuanto dinero desea retirar: ";
        cin>>retiro;
        if(retiro>saldoini){
            cout<<"no cuenta con esa cantidad";
        }else{
            saldo=saldoini-retiro;
            cout<<"\nDinero en cuenta: "<<saldo;
            break;
        }
        case 3: cout<<"su saldo es: "<<saldoini;break;
        case 4: break;
        default: cout<<"opcion incorrecta "; break;
    }
    return 0;
}
